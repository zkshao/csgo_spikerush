#include <sourcemod>
#include <smlib>
#include <cstrike>
#include <sdktools_functions>
#include <sdktools>
#include <sdkhooks>
#include <vector>
 
public Plugin myinfo =
{
	name = "Spy Crush",
	author = "Zhi Kang Shao",
	description = "A novel, never before seen fast paced game mode",
	version = "1.0",
	url = "http://www.zkshao.com/"
};

bool 	gl_bPlayerWeaponJump			[MAXPLAYERS + 1];
int 	gl_iPlayerNewGrenadeTime		[MAXPLAYERS + 1];
float 	gl_vPlayerWeaponJumpVelocity	[MAXPLAYERS + 1][3];

// Entry point for plugin loaded on server start
public void OnPluginStart()
{
	PrintToServer("=========================================================");
	PrintToServer("	 GRENADE JUMP											");
	PrintToServer("															");
	PrintToServer("	 Author: Zhi Kang Shao									");
	PrintToServer("	 Repo: https://bitbucket.org/zkshao/csgo_spikerush/		");
	PrintToServer("=========================================================");
	
	// Documentation: https://wiki.alliedmods.net/Counter-Strike:_Global_Offensive_Events
	
	// Skip warmup when enough players spawned
	HookEvent("hegrenade_detonate", Event_GrenadeDetonate, EventHookMode_Pre);
}

public void OnClientConnected(int iClient)
{
	// Initialize the client data
	gl_bPlayerWeaponJump		[iClient]	 = false;
	gl_iPlayerNewGrenadeTime	[iClient]	 = 2147483647;
	gl_vPlayerWeaponJumpVelocity[iClient][0] = 0.0;
	gl_vPlayerWeaponJumpVelocity[iClient][1] = 0.0;
	gl_vPlayerWeaponJumpVelocity[iClient][2] = 0.0;
}

public void OnClientPutInServer(int iClient)
{
	// Hook the client postthink function
	SDKHook(iClient, SDKHook_PostThinkPost, OnPlayerPostThinkPost);
}

public void OnClientDisconnect(int iClient)
{
	// Clear the client data
	gl_bPlayerWeaponJump		[iClient]	 = false;
	gl_iPlayerNewGrenadeTime	[iClient]	 = 2147483647;
	gl_vPlayerWeaponJumpVelocity[iClient][0] = 0.0;
	gl_vPlayerWeaponJumpVelocity[iClient][1] = 0.0;
	gl_vPlayerWeaponJumpVelocity[iClient][2] = 0.0;
}

float gl_fGrenadeJumpKnockback 			= 400.0;
float gl_fGrenadeJumpVelocityDampen 	= 0.3;
float gl_fGrenadeJumpEffectRadius 		= 256.0;
float gl_fGrenadeJumpKnockbackZ 		= 200.0;
int gl_iGrenadeCooldown 				= 6;

public Action Event_GrenadeDetonate(Event event, const char[] name, bool dontBroadcast)
{
	// Schedule new grenade
	int iCurUser = event.GetInt("userid");
	int iCurPlayer = GetClientOfUserId(iCurUser);
	gl_iPlayerNewGrenadeTime[iCurPlayer] = GetTime() + gl_iGrenadeCooldown;
	PrintToServer("Scheduling new grenade for (user %d, player %d) at (time: %d)", iCurUser, iCurPlayer, gl_iPlayerNewGrenadeTime[iCurPlayer]);
	
	float vGrenadePos[3];
	vGrenadePos[0] = event.GetFloat("x");
	vGrenadePos[1] = event.GetFloat("y");
	vGrenadePos[2] = event.GetFloat("z");
	
	// Iterate all users
	for (new iUser = 1; iUser <= MaxClients; ++iUser)
	{
		// Skip unconnected users
		if (!IsClientInGame(iUser))
		{
			continue;
		}
		
		// Get player entity ID
		int iPlayer = GetClientOfUserId(iUser);
		
		// Get player position
		float vPlayerPosition[3];
		GetEntPropVector(iPlayer, Prop_Data, "m_vecOrigin", vPlayerPosition);
		
		// Only affect players close to the explosion
		float fDistSq = GetVectorDistance(vGrenadePos, vPlayerPosition, true);
		if (fDistSq < gl_fGrenadeJumpEffectRadius * gl_fGrenadeJumpEffectRadius)
		{
			// Get player velocity
			float vPlayerVelocity[3];
			GetEntPropVector(iPlayer, Prop_Data, "m_vecVelocity", vPlayerVelocity);
			
			float vOffset[3];
			float vUnit[3];
			SubtractVectors(vPlayerPosition, vGrenadePos, vOffset);
			NormalizeVector(vOffset, vUnit);
			
			// Compute the player weapon jump velocity
			gl_vPlayerWeaponJumpVelocity[iPlayer][0] = vPlayerVelocity[0] * gl_fGrenadeJumpVelocityDampen + vUnit[0] * gl_fGrenadeJumpKnockback;
			gl_vPlayerWeaponJumpVelocity[iPlayer][1] = vPlayerVelocity[1] * gl_fGrenadeJumpVelocityDampen + vUnit[1] * gl_fGrenadeJumpKnockback;
			gl_vPlayerWeaponJumpVelocity[iPlayer][2] = vPlayerVelocity[2] * gl_fGrenadeJumpVelocityDampen + vUnit[2] * gl_fGrenadeJumpKnockback + gl_fGrenadeJumpKnockbackZ;
			
			// Set the player weapon jump
			gl_bPlayerWeaponJump[iPlayer] = true;
			PrintToServer("Knocking back (user: %d, player: %d) by grenade from (user: %d, player: %d)", iUser, iPlayer, iCurUser, iCurPlayer);
		}
	}
	
	return Plugin_Continue;
}

public void OnPlayerPostThinkPost(int iPlayer)
{
	// Check if the player must weapon jump
	if (gl_bPlayerWeaponJump[iPlayer])
	{
		// Knockback the player
		TeleportEntity(iPlayer, NULL_VECTOR, NULL_VECTOR, gl_vPlayerWeaponJumpVelocity[iPlayer]);
		
		// Reset the player weapon jump
		gl_bPlayerWeaponJump[iPlayer] = false;
	}
	
	// Check if user should get new grenade
	if (GetTime() > gl_iPlayerNewGrenadeTime[iPlayer])
	{
		PrintToServer("Giving new grenade to (player: %d) at (time: %d)", iPlayer, gl_iPlayerNewGrenadeTime[iPlayer]);
		
		gl_iPlayerNewGrenadeTime[iPlayer] = 2147483647;
		GivePlayerItem(iPlayer, "weapon_hegrenade");
	}
}