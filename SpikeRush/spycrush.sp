#include <sourcemod>
#include <smlib>
#include <cstrike>
#include <sdktools_functions>
 
public Plugin myinfo =
{
	name = "Spy Crush",
	author = "Zhi Kang Shao",
	description = "A novel, never before seen fast paced game mode",
	version = "1.0",
	url = "http://www.zkshao.com/"
};

// Constants
#define NUM_ROUNDS_WINLIMIT 4
#define NUM_ROUNDS_SWAP 3
#define NUM_ROUNDS_EARLY 1

// https://wiki.alliedmods.net/Counter-Strike:_Global_Offensive_Weapons
char g_EarlyRoundWeapons[][64] = {
	"weapon_nova",
	"weapon_deagle",
	"weapon_usp_silencer",
	"weapon_tec9",
	"weapon_elite",
	"weapon_sawedoff",
	"weapon_taser",
	"weapon_revolver",
};

char g_LateRoundWeapons[][64] = {
	"weapon_famas",
	"weapon_mp9",
	"weapon_aug",
	"weapon_m4a1_silencer",
	"weapon_ak47",
	"weapon_negev",
	"weapon_xm1014",
	"weapon_ssg08",
	"weapon_awp",
	"weapon_scar20",
};

char g_LateRoundWeapons_Weak[][64] = {
	"weapon_tec9",
	"weapon_tec9",
	"weapon_tec9",
	"weapon_usp_silencer",
	"weapon_deagle",
	"weapon_elite",
	"weapon_nova",
	"weapon_usp_silencer",
	"weapon_ssg08",
	"weapon_ssg08",
};

// Declare boolean arrays for storing which weapons are used
bool g_UsedEarlyRoundWeapons[32];
bool g_UsedLateRoundWeapons[32];
bool warmupSkipped = false;
bool buyzonesRemoved = false;

char g_Abilities[][64] = {
	"weapon_hegrenade",
	//"weapon_flashbang",
	//"weapon_decoy",
	//"weapon_incgrenade",
	//"weapon_smokegrenade",
	//"weapon_shield",
};

// Entry point for plugin loaded on server start
public void OnPluginStart()
{
	PrintToServer("=========================================================");
	PrintToServer("  SPY CRUSH                                              ");
	PrintToServer("                                                         ");
	PrintToServer("  Author: Zhi Kang Shao                                  ");
	PrintToServer("  Repo: https://bitbucket.org/zkshao/csgo_spikerush/     ");
	PrintToServer("=========================================================");
	
	// Documentation: https://wiki.alliedmods.net/Counter-Strike:_Global_Offensive_Events
	
	// (DISABLED) Skip warmup when enough players spawned
	//HookEvent("player_spawn", Event_PlayerSpawned, EventHookMode_Pre);
	
	// Assign loadouts on round start
	HookEvent("round_poststart", Event_RoundStart, EventHookMode_Pre);
	// Check win condition and trigger announcements at round end
	HookEvent("round_end", Event_RoundEnd, EventHookMode_Post);
	// Swap team sides right before certain rounds
	HookEvent("round_prestart", Event_PreRoundStart, EventHookMode_Pre);
}

// Event when client connects
public Action Event_PlayerSpawned(Event event, const char[] name, bool dontBroadcast)
{
	if (GameRules_GetProp("m_bWarmupPeriod") == 1 && !warmupSkipped)
	{
		// Count num connected players
		int numClients = 0;
		for (new i = 1; i <= MaxClients; ++i)
		{
			// Ignore disconnected players
			if (IsClientInGame(i) && !IsFakeClient(i))
			{
				numClients++;
			}
		}

		// If enough players, skip warmup
		if (numClients > 3)
		{
			PrintToServer("SKIPPING WARMUP");
			ServerCommand("mp_warmuptime 5");
			ServerCommand("mp_warmup_end");
			warmupSkipped = true;
			
			RemoveBuyZones();
			buyzonesRemoved = true;
		}
	}
	
	return Plugin_Continue;
}

// Entry point for map start
public void OnMapStart()
{	
	// Initialize all weapons as available
	ResetUsedWeapons();
}

public void OnMapEnd()
{
	// Reset do-once bools
	warmupSkipped = false;
	buyzonesRemoved = false;
}

public void RemoveBuyZones()
{
	// Remove buy zones
	// Source: https://forums.alliedmods.net/showthread.php?p=1541253
	char className[64];
	for (new i = MaxClients; i <= GetMaxEntities(); ++i)
	{
		// Iterate all entities, remove entities with class 'func_buyzone'
		if (IsValidEdict(i) && IsValidEntity(i))
		{
			GetEdictClassname(i, className, sizeof(className));
			if (StrEqual("func_buyzone", className))
			{
				RemoveEdict(i);
			}
		}
	}
}

public void ResetUsedWeapons()
{
	PrintToServer("Spy Crush: Resetting weapons");
	for (new i = 0; i < 32; ++i)
	{
		g_UsedEarlyRoundWeapons[i] = false;
		g_UsedLateRoundWeapons[i] = false;
	}
}

public bool ShouldEndGame()
{
	return CS_GetTeamScore(CS_TEAM_T) >= NUM_ROUNDS_WINLIMIT || CS_GetTeamScore(CS_TEAM_CT) >= NUM_ROUNDS_WINLIMIT;
}

public bool ShouldSwapTeamSides()
{
	int roundIndex = GetRoundIndex();
	return roundIndex > 0 && (roundIndex % NUM_ROUNDS_SWAP) == 0;
}

public int GetRoundIndex()
{
	return CS_GetTeamScore(CS_TEAM_T) + CS_GetTeamScore(CS_TEAM_CT);
}

public void AnnounceSwapTeamSides()
{
	for (new i = 1; i <= MaxClients; ++i)
	{
		// Ignore disconnected players
		if (IsClientInGame(i))
		{
			// Client UI feedback
			PrintCenterText(i, "CHANGING SIDES");
		}		
	}
}

public void SwapTeamSides()
{
	// Change each player's team
	for (new i = 1; i <= MaxClients; ++i)
	{
		// Ignore disconnected players
		if (IsClientInGame(i))
		{
			// Change player's team
			int clientTeam = GetClientTeam(i);
			if (clientTeam == CS_TEAM_T)
			{
				ChangeClientTeam(i, CS_TEAM_CT);
			}
			else if (clientTeam == CS_TEAM_CT)
			{
				ChangeClientTeam(i, CS_TEAM_T);
			}
		}
	}
	
	// Swap team scores
	int scoreT = CS_GetTeamScore(CS_TEAM_T);
	int scoreCT = CS_GetTeamScore(CS_TEAM_CT);
	SetTeamScore(CS_TEAM_T, scoreCT)
	CS_SetTeamScore(CS_TEAM_T, scoreCT);
	SetTeamScore(CS_TEAM_CT, scoreT)
	CS_SetTeamScore(CS_TEAM_CT, scoreT);
}

public Action Event_RoundStart(Event event, const char[] name, bool dontBroadcast)
{
	// Don't affect loadout during warmup
	if (GameRules_GetProp("m_bWarmupPeriod") == 1)
	{
		return Plugin_Continue;
	}
	
	if (!buyzonesRemoved)
	{
		PrintToServer("REMOVING BUY ZONES");
		RemoveBuyZones();
		buyzonesRemoved = true;
	}

	// GetRandomInt isn't very random, benefits from regular new seed
	SetRandomSeed(RoundFloat(GetEngineTime()));
	
	// Choose a random weapon for the round
	char randomWeaponID[64];
	char weakWeaponID[64];
	bool isEarlyRound = (GetRoundIndex() % NUM_ROUNDS_SWAP) < NUM_ROUNDS_EARLY;
	if (isEarlyRound)
	{
		// Reroll weapon index until encountering a new one
		int randomWeaponIndex;
		do
		{
			randomWeaponIndex = GetRandomInt(0, sizeof(g_EarlyRoundWeapons) - 1);
		}
		while (g_UsedEarlyRoundWeapons[randomWeaponIndex]);
		
		// Mark weapon as used then use its weaponID
		g_UsedEarlyRoundWeapons[randomWeaponIndex] = true;
		randomWeaponID = g_EarlyRoundWeapons[randomWeaponIndex];
		weakWeaponID = randomWeaponID;
	}
	else
	{
		// Reroll weapon index until encountering a new one
		int randomWeaponIndex;
		do
		{
			randomWeaponIndex = GetRandomInt(0, sizeof(g_LateRoundWeapons) - 1);
		}
		while (g_UsedLateRoundWeapons[randomWeaponIndex]);
		
		// Mark weapon as used then use its weaponID
		g_UsedLateRoundWeapons[randomWeaponIndex] = true;
		randomWeaponID = g_LateRoundWeapons[randomWeaponIndex];
		weakWeaponID = g_LateRoundWeapons_Weak[randomWeaponIndex];
	}
	
	// Choose a random ability for the round
	int randomAbilityIndex = GetRandomInt(0, sizeof(g_Abilities) - 1);
	char randomAbilityID[64];
	bool giveSameAbility = GetRandomInt(0, 3) == 3;
	if (giveSameAbility)
	{
		randomAbilityID = g_Abilities[randomAbilityIndex];
	}
	
	// Calc each team's current MVP score
	int mvpScore[4] = {0, 0, 0, 0};
	for (new i = 1; i <= MaxClients; ++i)
	{		
		// Ignore disconnected players
		if (IsClientInGame(i))
		{
			int score = CS_GetClientContributionScore(i);
			int team = GetClientTeam(i);
			if (score > mvpScore[team])
			{
				mvpScore[team] = score;
			}
		}
	}
	
	// Check if map name contains 'de_' prefix at index 0
	char currentMapName[64];
	GetCurrentMap(currentMapName, sizeof(currentMapName));
	bool isBombScenario = StrContains(currentMapName, "de_", false) == 0;
		
	// Iterate over all players (includes bots) to apply inventory
	for (new i = 1; i <= MaxClients; ++i)
	{		
		// Ignore disconnected players
		if (IsClientInGame(i))
		{
			// Remove all default weapons except knife
			Client_RemoveAllWeapons(i, "weapon_knife", true);
		
			// On bomb scenarios all terrorists get C4
			int team = GetClientTeam(i);
			if (isBombScenario && team == CS_TEAM_T)
			{
				GivePlayerItem(i, "weapon_c4", 0);
			}
			
			// If this round players get unique abilities, reroll ability
			if (!giveSameAbility)
			{
				randomAbilityIndex = GetRandomInt(0, sizeof(g_Abilities) - 1);
				randomAbilityID = g_Abilities[randomAbilityIndex];
			}
			
			// Give the random gear
			if (mvpScore[team] > 0 && CS_GetClientContributionScore(i) == mvpScore[team])
			{
				GivePlayerItem(i, weakWeaponID);
				
				// Client UI feedback
				PrintHintText(i, "DEBUFFED! YOU GET: %s. Others get: %s", weakWeaponID, randomWeaponID);
			}
			else
			{
				GivePlayerItem(i, randomWeaponID);
				
				// Client UI feedback
				PrintHintText(i, "Weapon of the Round: %s", randomWeaponID);
			}
			GivePlayerItem(i, randomAbilityID);
			
		}
		
	}
	
	return Plugin_Continue;
}

public Action Event_RoundEnd(Event event, const char[] name, bool dontBroadcast)
{
	if (ShouldEndGame())
	{
		// Schedule ending of game
		Game_End();
	}
	else if (ShouldSwapTeamSides())
	{
		// Announce that sides will be changed soon
		AnnounceSwapTeamSides();
	}
	
	return Plugin_Continue;
}

public Action Event_PreRoundStart(Event event, const char[] name, bool dontBroadcast)
{
	if (ShouldEndGame())
	{
		//
	}
	else if (ShouldSwapTeamSides())
	{
		// Change the sides
		SwapTeamSides();
	}
	
	return Plugin_Continue;
}
